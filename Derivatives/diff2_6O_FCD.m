function [d2] = diff2_6O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients:1/90	?3/20	3/2	?49/18	3/2	?3/20	1/90	
n = length(x); %the 4th deriv will have size n-6, should be able to get values for 4:n-3
d2 = ((1/90)*x(1:n-6) + (-3/20)*x(2:n-5) + (3/2)*x(3:n-4) + (-49/18)*x(4:n-3) + (3/2)*x(5:n-2) + (-3/20)*x(6:n-1) +(1/90)*x(7:n))./h^2;

end

