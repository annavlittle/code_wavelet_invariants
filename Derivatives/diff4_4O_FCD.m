function [d4] = diff4_4O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients:?1/6	2	?13/2	28/3	?13/2	2	?1/6	
n = length(x); %the 4th deriv will have size n-6, should be able to get values for 4:n-3
d4 = ((-1/6)*x(1:n-6) + (2)*x(2:n-5) + (-13/2)*x(3:n-4) + (28/3)*x(4:n-3) + (-13/2)*x(5:n-2) + (2)*x(6:n-1) +(-1/6)*x(7:n))./h^4;

end

