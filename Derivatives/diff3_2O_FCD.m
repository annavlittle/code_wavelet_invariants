function [d3] = diff3_2O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients: ?1/2 	1	0	?1	1/2
n = length(x); %the 4th deriv will have size n-4

%should be able to get derivs for points 3:n-2. I'll need these values:
d3 = ((-1/2)*x(1:n-4) + (1)*x(2:n-3) + (0)*x(3:n-2) + (-1)*x(4:n-1) + (1/2)*x(5:n))./h^3;

end

