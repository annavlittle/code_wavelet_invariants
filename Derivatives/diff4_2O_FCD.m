function [d4] = diff4_2O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients: 1 ?4	 6	?4	1
n = length(x); %the 4th deriv will have size n-4

%should be able to get derivs for points 3:n-2. I'll need these values:
d4 = (x(1:n-4) - 4*x(2:n-3) + 6*x(3:n-2) -4*x(4:n-1) + x(5:n))./h^4;

end

