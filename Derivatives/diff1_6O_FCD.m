function [d1] = diff1_6O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients:?1/60	3/20	?3/4	0	3/4	?3/20	1/60	
n = length(x); %the 4th deriv will have size n-6, should be able to get values for 4:n-3
d1 = ((-1/60)*x(1:n-6) + (3/20)*x(2:n-5) + (-3/4)*x(3:n-4) + (0)*x(4:n-3) + (3/4)*x(5:n-2) + (-3/20)*x(6:n-1) +(1/60)*x(7:n))./h;

end

