addpath(genpath('Derivatives'))
addpath(genpath('FilterBank'))

%% Select example: 

Signal = input(['\nWhich example do you want to run?',...
       '\n ',...
       '\n1 Low Frequency Gabor',...
       '\n ',...
       '\n2 Medium Frequency Gabor',...
       '\n ',...
       '\n3 High Frequency Gabor',...
       '\n ',...
       '\n4 Discontinuous Power Spectrum',...
       '\n ',...
       '\n5 Slowly Decaying Power Spectrum',...
       '\n ',...
       '\n6 High Frequency Chirp',...
       '\n ',...
       '\nInput example number without brackets or parentheses: ']);

if Signal==1
    
    f1 = @(x)exp(-5*x.^2).*cos(8.*x);
    RandomDilationOpts.SynthesisDomain = 'Space';
    RandomDilationOpts.Translate = 'True';
    
elseif Signal==2
    
    f1 = @(x)exp(-5*x.^2).*cos(16.*x);
    RandomDilationOpts.SynthesisDomain = 'Space';
    RandomDilationOpts.Translate = 'True';
    
elseif Signal==3
    
    f1 = @(x)exp(-5*x.^2).*cos(32.*x);
    RandomDilationOpts.SynthesisDomain = 'Space';
    RandomDilationOpts.Translate = 'True';
    
elseif Signal==4
    
    f1 = @(x)(0.384)*(step_function(x,-38,-32)+step_function(x,32,38));
    RandomDilationOpts.SynthesisDomain = 'Frequency';
    RandomDilationOpts.Translate = 'False';
    
elseif Signal==5
    
    f1 = @(x)(0.47)*(sinc(.2.*(x-32))+sinc(.2.*(-x-32)));
    RandomDilationOpts.SynthesisDomain = 'Frequency';
    RandomDilationOpts.Translate = 'False';
    
elseif Signal==6

    f1 = @(x)0.299*exp(-.04*(x).^2).*cos(30*(x)+1.5*x.^2);
    RandomDilationOpts.SynthesisDomain = 'Space';
    RandomDilationOpts.Translate = 'True';
    
end

%% Set Parameters  

N=2^(4); %Signal is defined on [-N/2, N/2), noise on [-N,N), with spacing 1/2^l; choose N at least 8 and a power of 2
l=5; %Frequencies are computed on [-(2^l)*pi, (2^l)*pi]
M=10000; % number of times we sample the noisy signal
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=2*1024; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; %don't use (only for exponential
%spacing)
RandomDilationOpts.MagnitudeMaxTau = 0.2; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 4; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 0; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.MomentCalc='Oracle';
%RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 4; %if RandomDilationOpts.MomentCalc='Empirical'
%MomentEstimationOpts.Method = 'PS'; % only use this with no additive noise
MomentEstimationOpts.Method = 'FT'; % only use this with no translations
true_noise_sigma = 2^(-4); %Optional: add additive Gaussian noise
OptimizationOpts.Method = 'Unconstrained'; %Note: do not change this setting; option 'Constrained' is not supported in current code
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;

% Create Filter Bank
[FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

RandomDilationsWithOptimization_LinearSpacing

%% Plot unbiasing results

figure

subplot(2,2,1);
plot(w,UndilatedPowerSpectrum,'LineWidth',2,'Color',[0.4940, 0.1840, 0.5560]);
xlabel('$\omega$','fontsize',14,'Interpreter','Latex');
title('Power Spectrum of Target Signal','fontsize',14,'Interpreter','Latex')
xlim([min(w) max(w)])

subplot(2,2,2);
hold on
for i=1:min(M,10)
    plot(w, PowerSpectrum(i,:))
end
xlim([min(w) max(w)])
xlabel('$\omega$','fontsize',14,'Interpreter','Latex');
title('Power Spectrum for $M=10$ Observed Signals','fontsize',14,'Interpreter','Latex')
plot(w, UndilatedPowerSpectrum,'Color',[0.4940, 0.1840, 0.5560],'LineWidth',3)

pos = get(gcf,'position');
set(gcf,'position',[pos(1:2)/4 pos(3:4)*2])

subplot(2,2,3);
plot(w(4:end-3), ZeroOrderUnbiasedPowerSpectrum,'-','Linewidth',2,'Color',[0, 0.4470, 0.7410])
hold on
if RandomDilationOpts.PSUnbiasingOrder == 0
    plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\omega$','fontsize',14,'Interpreter','Latex');
    title({'Power Spectrum Estimators $\widetilde{Pf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'PS $k=0$', 'Target PS'},'fontsize',14,'Interpreter','Latex')
    legend('Location','northeast')
elseif RandomDilationOpts.PSUnbiasingOrder == 2
    plot(w(4:end-3),SecondOrderUnbiasedPowerSpectrum,'-','Linewidth',2,'Color',[0.3010, 0.7450, 0.9330])
    plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\omega$','fontsize',14,'Interpreter','Latex');
    title({'Power Spectrum Estimators $\widetilde{Pf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'PS $k=0$', 'PS $k=2$','Target PS'},'FontSize',14,'Interpreter','Latex')
    legend('Location','northeast')
elseif RandomDilationOpts.PSUnbiasingOrder == 4
    plot(w(4:end-3),SecondOrderUnbiasedPowerSpectrum,'-','Linewidth',2,'Color',[0.3010, 0.7450, 0.9330])
    plot(w(4:end-3),FourthOrderUnbiasedPowerSpectrum,'-','Linewidth',2,'Color',[0.4660, 0.6740, 0.1880])
    plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\omega$','fontsize',14,'Interpreter','Latex');
    title({'Power Spectrum Estimators $\widetilde{Pf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'PS $k=0$', 'PS $k=2$', 'PS $k=4$','Target PS'},'FontSize',14,'Interpreter','Latex')
    legend('Location','northeast')
end

subplot(2,2,4);
plot(lam(4:end-3), ZeroOrderUnbiasedFirstOrderWSC,'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
hold on
if RandomDilationOpts.WSCUnbiasingOrder == 0
    plot(lam(4:end-3),UndilatedFirstOrderWSC(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\lambda$','fontsize',14,'Interpreter','Latex');
    title({'Wavelet Invariant Estimators $\widetilde{Sf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'WSC $k=0$','Target WSC'},'FontSize',14,'Interpreter','Latex')
    legend('Location','northeast')
elseif RandomDilationOpts.WSCUnbiasingOrder == 2
    plot(lam(4:end-3),SecondOrderUnbiasedFirstOrderWSC,'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
    plot(lam(4:end-3),UndilatedFirstOrderWSC(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\lambda$','fontsize',14,'Interpreter','Latex');
    title({'Wavelet Invariant Estimators $\widetilde{Sf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'WSC $k=0$','WSC $k=2$','Target WSC'},'FontSize',14,'Interpreter','Latex')
    legend('Location','northeast')    
elseif RandomDilationOpts.WSCUnbiasingOrder == 4
    plot(lam(4:end-3),SecondOrderUnbiasedFirstOrderWSC,'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
    plot(lam(4:end-3),FourthOrderUnbiasedFirstOrderWSC,'-','Linewidth',2,'Color',[0.9290, 0.6940, 0.1250])
    plot(lam(4:end-3),UndilatedFirstOrderWSC(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
    xlabel('$\lambda$','fontsize',14,'Interpreter','Latex');
    title({'Wavelet Invariant Estimators $\widetilde{Sf}$'},'fontsize',14,'Interpreter','Latex')
    legend({'WSC $k=0$','WSC $k=2$','WSC $k=4$','Target WSC'},'FontSize',14,'Interpreter','Latex')
    legend('Location','northeast')
end
xlim([min(lam) max(lam)])

%% Plot recovered power spectrum for PS and WSC+optimization methods:

if RandomDilationOpts.PSUnbiasingOrder == 0
    recoveredfPSPS = ZeroOrderUnbiasedPowerSpectrum;
    plot_color1 = [0.6350, 0.0780, 0.1840];
elseif RandomDilationOpts.PSUnbiasingOrder == 2
    recoveredfPSPS = SecondOrderUnbiasedPowerSpectrum;
    plot_color1 = [0.8500, 0.3250, 0.0980];
elseif RandomDilationOpts.PSUnbiasingOrder == 4
    recoveredfPSPS = FourthOrderUnbiasedPowerSpectrum;
    plot_color1 = [0.9290, 0.6940, 0.1250];
end

if RandomDilationOpts.PSUnbiasingOrder == 0
    recoveredfPSPS = ZeroOrderUnbiasedPowerSpectrum;
    plot_colorPS = [0, 0.4470, 0.7410];
elseif RandomDilationOpts.PSUnbiasingOrder == 2
    recoveredfPSPS = SecondOrderUnbiasedPowerSpectrum;
    plot_colorPS = [0.3010, 0.7450, 0.9330];
elseif RandomDilationOpts.PSUnbiasingOrder == 4
    recoveredfPSPS = FourthOrderUnbiasedPowerSpectrum;
    plot_colorPS = [0.4660, 0.6740, 0.1880];
end

if RandomDilationOpts.WSCUnbiasingOrder == 0
    plot_colorWSC = [0.6350, 0.0780, 0.1840];
elseif RandomDilationOpts.WSCUnbiasingOrder == 2
    plot_colorWSC = [0.8500, 0.3250, 0.0980];
elseif RandomDilationOpts.WSCUnbiasingOrder == 4
    plot_colorWSC = [0.9290, 0.6940, 0.1250];
end


figure
plot(w(4:end-3),UndilatedPowerSpectrum(4:end-3),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
hold on
grid on
axis square
plot(w(4:end-3),recoveredfPSPS,'Linewidth',2,'Color',plot_colorPS)
plot(w(4:end-3),recoveredfPS(4:end-3),'LineWidth',2,'Color',plot_colorWSC)
xlim([min(w) max(w)])
xlabel('$\omega$','fontsize',14,'Interpreter','Latex')
title({'Recovered Power Spectra ($\widetilde{Pf}$ and $\widetilde{P_Sf}$)'},'fontsize',14,'Interpreter','Latex')
legend({'Target PS',['PS $k=$ ',num2str(RandomDilationOpts.PSUnbiasingOrder),''],['WSC $k=$ ',num2str(RandomDilationOpts.WSCUnbiasingOrder),'']},'Interpreter','Latex','FontSize',18)
legend('Location','northeast')


