function [psi_hat_analytic] = CubicSplineMotherWaveletFT(w)
%Function which constructs the FT of the cubic spline mother wavelet
%described in Ex. 7.9 of Mallat; here the mother wavelet is considered to
%be psi_hat(2w) for the psi_hat from text so that the essential support is in [-pi/2,pi] instead of
%[pi,2pi]; we also consider just the analytic part of this wavelet
    m = length(w);
    w = 1.2*w; %dilate a little bit more for better concentration in [-pi/2,pi]
    h_hat = sqrt(CubicSpline(w+pi)./((2^7)*CubicSpline(2*w+2*pi))); % Note: h_hat gives h_hat at w/2+pi
    phi_hat = ((w.^4).*sqrt(CubicSpline(w))).^(-1); % Note: phi_hat gives phi_hat at w/2
    psi_hat = (1/sqrt(2)).*exp(-1j.*w).*conj(h_hat).*phi_hat;%psi_hat(w) = (1/sqrt(2))*exp(-iw)*conj(h_hat(w+pi))*phi_hat(w)
    % Construct an analytic wavelet from the above:
    psi_hat_analytic = zeros(1,m);
    pos_idx = find(w>0);
    neg_idx = find(w<=0);
    psi_hat_analytic(neg_idx) = zeros(size(neg_idx));
    psi_hat_analytic(pos_idx) = 2*psi_hat(pos_idx);
end

