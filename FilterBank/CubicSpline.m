function [S8] = CubicSpline(w)
%Function which computes the cubic spline function S_8(w) given in (7.27)
%of Mallat
zero_idx = find(w==0);
nonzero_idx = find(w~=0);
S8(nonzero_idx) = (5+30.*(cos(w(nonzero_idx)/2).^2)+30.*(sin(w(nonzero_idx)/2).*cos(w(nonzero_idx)/2)).^2)./(105.*(2.*sin(w(nonzero_idx)/2)).^8) + (70.*(cos(w(nonzero_idx)/2).^4)+2.*(sin(w(nonzero_idx)/2).^4).*(cos(w(nonzero_idx)/2).^2)+(2/3).*(sin(w(nonzero_idx)/2).^6))./(105.*(2.*sin(w(nonzero_idx)/2)).^8);
S8(zero_idx) = zeros(size(zero_idx));
end

