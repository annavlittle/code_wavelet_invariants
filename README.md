## Read Me for Derivative Unbiasing with Wavelet Invariants Code Repository 

This repository contains code to reproduce the numerical results reported in the article "Wavelet invariants for statistically robust multi-reference alignment" by Matthew Hirn and Anna Little (Information and Inference, 2020). The code was written using Matlab 9.3.0.713579 (R2017b).

The script RunExamples illustrates the unbiasing procedure and power spectrum recovery for six example signals. With the Matlab search path set to the main folder, simply run:

```
>> RunExamples
```

The user will then see a prompt allowing them to select the example they wish to run. The default parameter settings are M=20,000 signals, additive noise level sigma = 2^(-4), uniform dilation distribution with standard deviation eta = 0.12, k=4th order wavelet invariant unbiasing, and oracle moment estimation. The user can adjust these options in the "Set Parameters" portion of the script.

The script MakeFigures calls the scripts contained in the Figure_scripts folder, which generate all figures in the article.  The numbering of the script names matches the numbering in the article. Scripts which take a long time to run also have a version with a demo extension which create a subfigure of the full figure; these can be run in a few minutes. WARNING: do not run MakeFigures all at once, as generating all figures takes a large amount of time and computing power. 

To run the figure scripts from the main folder, first add the Figure_scripts folder with subfolders to the Matlab search path (see Line 3 of MakeFigures), then call any figure script. For example:

```
>> addpath(genpath('Figure_scripts'));

>> fig2_b_demo
```

One can also navigate to the relevant subfolder Figure_scripts/Figure_2, and run the script without adding the folder to the search path.
