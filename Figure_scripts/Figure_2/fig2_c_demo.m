addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
M = 500;
RandomDilationOpts.Translate = 'True'; %Only use 'True' when SynthesisDomain = 'Space'!
RandomDilationOpts.Distribution='NoDilation';
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=100; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; 
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.MagnitudeMaxTau = 0.2; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
%RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 0; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 0; %options: 0,2,4 
%RandomDilationOpts.MomentCalc='Oracle';
RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Method = 'Mean'; % only for Empirical moment estimation
%MomentEstimationOpts.Method = 'Median';
sigma_min = 2^(-7);
sigma_max = 2^(-2);
SigmaValues = 2.^(linspace(log2(sigma_min), log2(sigma_max), 3));
NumberSimulationsPerValue = 3;
%OptimizationOpts.Method = 'Constrained';
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
%OptimizationOpts.tol = ErrorSecOrderUnbiasing^2;
OptimizationOpts.ThresholdTargetWSC = 'no';
OptimizationOpts.Threshold = 1e-5;
%OptimizationOpts.Threshold = 2*N^2*true_noise_sigma^4/M; %1e-5;
OptimizationOpts.tol = 1e-7;

% Med freq chirp:
f1 = @(x)exp(-5*x.^2).*cos(16.*x);
%f1 = @(x)exp(-.04*(x).^2).*cos(16*x+1.3*x.^2);   % works well; WSC slightly better than PS; PS little jagged

% Create Filter Bank
[FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);


Error_ZeroOrderPS = zeros( length(SigmaValues), 1);
Error_SecondOrderPS = zeros( length(SigmaValues), 1);
Error_FourthOrderPS = zeros( length(SigmaValues), 1);
Error_ZeroOrderWSCOpt = zeros( length(SigmaValues), 1);
Error_SecondOrderWSCOpt = zeros( length(SigmaValues), 1);
Error_FourthOrderWSCOpt = zeros( length(SigmaValues), 1);
SDError_ZeroOrderPS = zeros( length(SigmaValues), 1);
SDError_SecondOrderPS = zeros( length(SigmaValues), 1);
SDError_FourthOrderPS = zeros( length(SigmaValues), 1);
SDError_ZeroOrderWSCOpt = zeros( length(SigmaValues), 1);
SDError_SecondOrderWSCOpt = zeros( length(SigmaValues), 1);
SDError_FourthOrderWSCOpt = zeros( length(SigmaValues), 1);
SDLogError_ZeroOrderPS = zeros( length(SigmaValues), 1);
SDLogError_SecondOrderPS = zeros( length(SigmaValues), 1);
SDLogError_FourthOrderPS = zeros( length(SigmaValues), 1);
SDLogError_ZeroOrderWSCOpt = zeros( length(SigmaValues), 1);
SDLogError_SecondOrderWSCOpt = zeros( length(SigmaValues), 1);
SDLogError_FourthOrderWSCOpt = zeros( length(SigmaValues), 1);
MeanMoments = zeros( length(SigmaValues), 2);

for s=1:length(SigmaValues)
    true_noise_sigma = SigmaValues(s);
    temp_PS_0 = zeros( NumberSimulationsPerValue, 1);
    temp_WSC_0 = zeros( NumberSimulationsPerValue, 1);
    for q=1:NumberSimulationsPerValue
        RandomDilationsWithOptimization_LinearSpacing
        temp_PS_0(q) = ErrorZeroOrderUnbiasingPS;
        temp_WSC_0(q) = PSerror_ZeroOrderWSCOpt;
    end
    Error_ZeroOrderPS(s) = mean(temp_PS_0);
    Error_ZeroOrderWSCOpt(s) = mean(temp_WSC_0);
    SDError_ZeroOrderPS(s) = std(temp_PS_0);
    SDError_ZeroOrderWSCOpt(s) = std(temp_WSC_0);
    SDLogError_ZeroOrderPS(s) = std(log2(temp_PS_0));
    SDLogError_ZeroOrderWSCOpt(s) = std(log2(temp_WSC_0));
end

%% Plot Results

plot_idx = 1:length(SigmaValues);
h=figure
errorbar(log2(SigmaValues(plot_idx)), log2(Error_ZeroOrderWSCOpt(plot_idx)),SDLogError_ZeroOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
xlim([min(log2(SigmaValues(plot_idx))) max(log2(SigmaValues(plot_idx)))])
axis square
grid on
hold on
errorbar(log2(SigmaValues(plot_idx)), log2(Error_ZeroOrderPS(plot_idx)), SDLogError_ZeroOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0, 0.4470, 0.7410])
xlabel('$\log_2(\sigma)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')
legend({'WSC, $k = 0$','PS, $k = 0$'},'FontSize',18,'Interpreter','latex')
legend('Location','southeast')




    
