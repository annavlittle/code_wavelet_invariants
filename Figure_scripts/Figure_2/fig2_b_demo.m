addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
%Mvalues = 2.^(4:1:17);
Mvalues = [1000 2000 4000];
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
true_noise_sigma = 2^(-3);

RandomDilationOpts.Translate = 'True'; %Only use 'True' when SynthesisDomain = 'Space'!
RandomDilationOpts.Distribution='NoDilation';

FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=100; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; 
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
%RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
RandomDilationOpts.WSCUnbiasingOrder = 0; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 0; %options: 0,2,4 
RandomDilationOpts.MomentCalc='Empirical'; %only for additive noise
NumberSimulationsPerValue = 3;
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
%OptimizationOpts.tol = ErrorSecOrderUnbiasing^2;
OptimizationOpts.tol = 1e-7;


% Low frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(8.*x);

% Medium frequency example:
f1 = @(x)exp(-5*x.^2).*cos(16.*x);

% High frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(32.*x);


%%
CombNoise_VaryM

%save('sim_AddNoise_varyM.mat','MeanMoments','Mvalues','NumberSimulationsPerValue','Error_ZeroOrderPS','Error_SecondOrderPS','Error_FourthOrderPS', 'SDError_ZeroOrderPS','SDError_SecondOrderPS','SDError_FourthOrderPS','SDLogError_ZeroOrderPS','SDLogError_SecondOrderPS','SDLogError_FourthOrderPS', 'Error_ZeroOrderWSCOpt','Error_SecondOrderWSCOpt','Error_FourthOrderWSCOpt', 'SDError_ZeroOrderWSCOpt','SDError_SecondOrderWSCOpt','SDError_FourthOrderWSCOpt','SDLogError_ZeroOrderWSCOpt','SDLogError_SecondOrderWSCOpt','SDLogError_FourthOrderWSCOpt')

%% Plot results

plot_idx = 1:length(Mvalues);
h=figure
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderWSCOpt(plot_idx)),SDLogError_ZeroOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
axis square
grid on
hold on
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderPS(plot_idx)), SDLogError_ZeroOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0, 0.4470, 0.7410])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')
legend({'WSC, $k = 0$','PS, $k = 0$'},'FontSize',18,'Interpreter','latex')
legend('Location','northeast')

    
