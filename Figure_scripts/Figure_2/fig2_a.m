N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
true_noise_sigma = 2^(-3);

% Medium frequency example:
f1 = @(x)exp(-5*x.^2).*cos(16.*x);

t1=-(N/2):(1/2^l):(N/2)-1/2^l;
t = -(N):(1/2^l):(N)-1/2^l;
w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);
f = [zeros(1,(2^l)*N/2) f1(t1) zeros(1,(2^l)*N/2)];
NoisySignal = f + true_noise_sigma*sqrt(2^l)*randn( size(f) );
FT = ifftshift(fft(fftshift(f)))*(1/2^l);
NoisyFT = ifftshift(fft(fftshift(NoisySignal)))*(1/2^l);
PS = abs(FT).^2;
NoisyPS = abs(NoisyFT).^2;

h=figure
plot(w,NoisyPS,'Linewidth',2,'Color',[0, 0.4470, 0.7410])
hold on
axis square
plot(w,PS, 'Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
xlim([min(w), max(w)])
xlabel('$\omega$','Fontsize',18,'Interpreter','latex')
legend({'Corrupted PS','Target PS'},'FontSize',18,'Interpreter','latex')
legend('Location','northeast')