addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

%% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
%N=ceil(2*pi*2^l);
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
M=20000; % number of times we sample the noisy signal
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=500; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; 
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency';
RandomDilationOpts.Translate = 'False';
RandomDilationOpts.MagnitudeMaxTau = 0.2; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 4; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 4; %options: 0,2,4 
%RandomDilationOpts.MomentCalc='Oracle';
RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 4;
MomentEstimationOpts.Method = 'FT'; % only for Empirical moment estimation
true_noise_sigma = 2^(-4); %Optional: add additive Gaussian noise
%OptimizationOpts.Method = 'Constrained';
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;

% Examples for paper:

% Low frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(8.*x);

% Medium frequency example:
f1 = @(x)exp(-5*x.^2).*cos(32.*x);

[FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

RandomDilationsWithOptimization_LinearSpacing

%% Plot recovered signals:

h=figure
plot_idx = intersect(find(t<3),find(t>-3));

ul = 1.05*max([max(real(f)) max(real(recoveredf)) max(real(recoveredf_AvgPS))]);
ll = 1.05*min([min(real(f)) min(real(recoveredf)) min(real(recoveredf_AvgPS))]);

plot(t(plot_idx),f(plot_idx),'LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
axis square
grid on
%title('Original Signal','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('Target Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

h=figure
plot(t(plot_idx),real(recoveredf(plot_idx)),'Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
axis square
grid on
%title('WSC Recovered Signal','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('WSC Recovered Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

h=figure
plot(t(plot_idx),real(recoveredf_AvgPS(plot_idx)),'Linewidth',2,'Color',[0, 0.4470, 0.7410])
axis square
grid on
%title('PS Recovered Signal','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('PS Recovered Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

h=figure
PS_plot_idx = intersect(find(w<52),find(w>12));
plot(w(PS_plot_idx),recoveredfPS(PS_plot_idx),'Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
hold on
grid on
axis square
xlim([min(w(PS_plot_idx)),max(w(PS_plot_idx))])
plot(w(PS_plot_idx),MeanPowerSpectrum(PS_plot_idx)-2*N*noise_sigma^2,'Linewidth',2,'Color',[0, 0.4470, 0.7410])
plot(w(PS_plot_idx),UndilatedPowerSpectrum(PS_plot_idx),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
xlabel('$\omega$','Fontsize',18,'Interpreter','latex')
legend({'$(\widetilde{P_{\mathcal{S}}f})(\omega)$','$(\widetilde{Pf})(\omega)$','Target PS'},'FontSize',18,'Interpreter','latex')
legend('Location','northeast')

