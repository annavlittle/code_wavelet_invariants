addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

% Parameter that will be adjusted:
true_noise_sigma = 2^(-3);
RandomDilationOpts.MagnitudeMaxTau = 0.2; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.MomentCalc='Oracle';
%RandomDilationOpts.MomentCalc='Empirical';
%Mvalues = 2.^(5:1:17);
Mvalues = 2.^(8:2:12);
RandomDilationOpts.Translate = 'True';

% High freq chirp:
f1 = @(x)0.299*exp(-.04*(x).^2).*cos(30*(x)+1.5*x.^2);

% Parameters we keep fixed:

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=100; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; 
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 4; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 4; %options: 0,2,4 
MomentEstimationOpts.Method = 'Mean'; % only for Empirical moment estimation
%MomentEstimationOpts.Method = 'Median';
NumberSimulationsPerValue = 3;
%OptimizationOpts.Method = 'Constrained';
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
%OptimizationOpts.tol = ErrorSecOrderUnbiasing^2;
OptimizationOpts.tol = 1e-7;

CombNoise_VaryM

%save('sim_O_6_chirp.mat','MeanMoments','Mvalues','NumberSimulationsPerValue','Error_ZeroOrderPS','Error_SecondOrderPS','Error_FourthOrderPS', 'SDError_ZeroOrderPS','SDError_SecondOrderPS','SDError_FourthOrderPS','SDLogError_ZeroOrderPS','SDLogError_SecondOrderPS','SDLogError_FourthOrderPS', 'Error_ZeroOrderWSCOpt','Error_SecondOrderWSCOpt','Error_FourthOrderWSCOpt', 'SDError_ZeroOrderWSCOpt','SDError_SecondOrderWSCOpt','SDError_FourthOrderWSCOpt','SDLogError_ZeroOrderWSCOpt','SDLogError_SecondOrderWSCOpt','SDLogError_FourthOrderWSCOpt')

PlotScriptOracleAdditional

chirp_range = [-8.5 -1];
ylim(chirp_range);


    
