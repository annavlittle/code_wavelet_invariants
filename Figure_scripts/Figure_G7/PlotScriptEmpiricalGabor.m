h=figure;
plot_idx = 1:1:length(Mvalues);
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderWSCOpt(plot_idx)),SDLogError_ZeroOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-8 -3])
axis square
grid on
hold on
errorbar(log2(Mvalues(plot_idx)), log2(Error_SecondOrderWSCOpt(plot_idx)), SDLogError_SecondOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
if Plot_WSC_4th_Order == 1
    errorbar(log2(Mvalues(plot_idx)), log2(Error_FourthOrderWSCOpt(plot_idx)),SDLogError_FourthOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.9290, 0.6940, 0.1250])
end
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderPS(plot_idx)), SDLogError_ZeroOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0, 0.4470, 0.7410])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')