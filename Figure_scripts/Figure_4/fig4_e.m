addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

% Parameter that will be adjusted:
true_noise_sigma = 0;
RandomDilationOpts.MagnitudeMaxTau = 0.2; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
%RandomDilationOpts.MomentCalc='Oracle';
RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 4;
Mvalues = 2.^(5:1:14);
%Mvalues = 2.^(8:2:12);
RandomDilationOpts.Translate = 'True';
MomentEstimationOpts.Method = 'PS';

% Low frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(8.*x);

% Medium frequency example:
f1 = @(x)exp(-5*x.^2).*cos(16.*x);

% High frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(32.*x);

% Parameters we keep fixed:

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=100; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; 
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 4; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 4; %options: 0,2,4 
NumberSimulationsPerValue = 10;
%OptimizationOpts.Method = 'Constrained';
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
%OptimizationOpts.tol = ErrorSecOrderUnbiasing^2;
OptimizationOpts.tol = 1e-7;

CombNoise_VaryM

%save('sim_E_2_med_PS.mat','MeanMoments','Mvalues','NumberSimulationsPerValue','Error_ZeroOrderPS','Error_SecondOrderPS','Error_FourthOrderPS', 'SDError_ZeroOrderPS','SDError_SecondOrderPS','SDError_FourthOrderPS','SDLogError_ZeroOrderPS','SDLogError_SecondOrderPS','SDLogError_FourthOrderPS', 'Error_ZeroOrderWSCOpt','Error_SecondOrderWSCOpt','Error_FourthOrderWSCOpt', 'SDError_ZeroOrderWSCOpt','SDError_SecondOrderWSCOpt','SDError_FourthOrderWSCOpt','SDLogError_ZeroOrderWSCOpt','SDLogError_SecondOrderWSCOpt','SDLogError_FourthOrderWSCOpt')

%% Plot results

plot_idx = 1:length(Mvalues);
figure
E_2_range = [-12.5 -1.5];
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderWSCOpt(plot_idx)),SDLogError_ZeroOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
ylim(E_2_range);
axis square
grid on
hold on
errorbar(log2(Mvalues(plot_idx)), log2(Error_SecondOrderWSCOpt(plot_idx)), SDLogError_SecondOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
errorbar(log2(Mvalues(plot_idx)), log2(Error_FourthOrderWSCOpt(plot_idx)),SDLogError_FourthOrderWSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2,'Color',[0.9290, 0.6940, 0.1250])
errorbar(log2(Mvalues(plot_idx)), log2(Error_ZeroOrderPS(plot_idx)), SDLogError_ZeroOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0, 0.4470, 0.7410])
errorbar(log2(Mvalues(plot_idx)), log2(Error_SecondOrderPS(plot_idx)), SDLogError_SecondOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0.3010, 0.7450, 0.9330])
errorbar(log2(Mvalues(plot_idx)), log2(Error_FourthOrderPS(plot_idx)), SDLogError_FourthOrderPS(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2,'Color',[0.4660, 0.6740, 0.1880])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')
    
