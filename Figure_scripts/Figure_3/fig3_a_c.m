addpath(genpath('../../'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
%N=ceil(2*pi*2^l);
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
M=20000; % number of times we sample the noisy signal
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=500; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1';
RandomDilationOpts.Translate = 'True';
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.MagnitudeMaxTau = 0.1; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
RandomDilationOpts.WSCUnbiasingOrder = 4; %options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 4; %options: 0,2,4 
%RandomDilationOpts.MomentCalc='Oracle';
RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 4;
MomentEstimationOpts.Method = 'PS';
true_noise_sigma = 0; %Optional: add additive Gaussian noise
%OptimizationOpts.Method = 'Constrained';
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;

% Examples for paper:

% Low frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(8.*x);

% Medium frequency example:
%f1 = @(x)exp(-5*x.^2).*cos(16.*x);

% High frequency example:
f1 = @(x)exp(-5*x.^2).*cos(32.*x);

% Create Filter Bank
[FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

RandomDilationsWithOptimization_LinearSpacing %Note: can comment out optimization part for speed up

%% Plot k=0,2,4 unbiased PS and WSC (Large Dilations)

PS_plot_idx = (N*2^l+25):ceil(1.6*N*2^l);
WSC_plot_idx = 4:length(lam)/1.3;

h=figure
plot(w(PS_plot_idx), ZeroOrderUnbiasedPowerSpectrum(PS_plot_idx),'-','Linewidth',2,'Color',[0, 0.4470, 0.7410])
hold on
grid on
axis square
plot(w(PS_plot_idx),SecondOrderUnbiasedPowerSpectrum(PS_plot_idx),'-','Linewidth',2,'Color',[0.3010, 0.7450, 0.9330])
plot(w(PS_plot_idx),FourthOrderUnbiasedPowerSpectrum(PS_plot_idx),'-','Linewidth',2,'Color',[0.4660, 0.6740, 0.1880])
plot(w(PS_plot_idx), UndilatedPowerSpectrum(PS_plot_idx),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
xlim([min(w(PS_plot_idx)), max(w(PS_plot_idx))])
xlabel('$\omega$','Fontsize',18,'Interpreter','latex')
ylabel('$(\widetilde{Pf})(\omega)$','Fontsize',18,'Interpreter','latex')
%title({'Target PS and 0th, 2nd, 4th Order Estimators'},'fontsize',14)
legend({'0th order est.', '2nd order est', '4th order est.','Target PS'},'FontSize',14)
legend('Location','northeast')

% h=figure
% plot(lam(WSC_plot_idx), ZeroOrderUnbiasedFirstOrderWSC(WSC_plot_idx),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
% hold on
% grid on
% axis square
% plot(lam(WSC_plot_idx),SecondOrderUnbiasedFirstOrderWSC(WSC_plot_idx),'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
% plot(lam(WSC_plot_idx),FourthOrderUnbiasedFirstOrderWSC(WSC_plot_idx),'-','Linewidth',2,'Color',[0.9290, 0.6940, 0.1250])
% plot(lam(WSC_plot_idx),UndilatedFirstOrderWSC(WSC_plot_idx),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
% xlim([min(lam(WSC_plot_idx)), max(lam(WSC_plot_idx))])
% xlabel('$\lambda$','Fontsize',18,'Interpreter','latex')
% ylabel('$(\widetilde{\mathcal{S}f})(\lambda)$','Fontsize',18,'Interpreter','latex')
% %title({'Target WSC and 0th, 2nd, 4th Order Estimators'},'fontsize',14)
% legend({'0th order est.','2nd order est.','4th order est.','Target WSC'},'FontSize',14)
% legend('Location','northeast')

%%
h=figure
plot(w(PS_plot_idx), ZeroOrder_recoveredfPS(PS_plot_idx),'-','Linewidth',2,'Color',[0.6350, 0.0780, 0.1840])
hold on
grid on
axis square
plot(w(PS_plot_idx),SecondOrder_recoveredfPS(PS_plot_idx),'-','Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
plot(w(PS_plot_idx),FourthOrder_recoveredfPS(PS_plot_idx),'-','Linewidth',2,'Color',[0.9290, 0.6940, 0.1250])
plot(w(PS_plot_idx),UndilatedPowerSpectrum(PS_plot_idx),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
xlim([min(w(PS_plot_idx)), max(w(PS_plot_idx))])
xlabel('$\omega$','Fontsize',18,'Interpreter','latex')
ylabel('$(\widetilde{P_Sf})(\omega)$','Fontsize',18,'Interpreter','latex')
legend({'0th order est.', '2nd order est', '4th order est.','Target PS'},'FontSize',14)
legend('Location','northeast')

