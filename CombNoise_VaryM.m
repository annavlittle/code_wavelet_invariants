% Create Filter Bank
[FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

Error_ZeroOrderPS = zeros( length(Mvalues), 1);
Error_SecondOrderPS = zeros( length(Mvalues), 1);
Error_FourthOrderPS = zeros( length(Mvalues), 1);
Error_ZeroOrderWSCOpt = zeros( length(Mvalues), 1);
Error_SecondOrderWSCOpt = zeros( length(Mvalues), 1);
Error_FourthOrderWSCOpt = zeros( length(Mvalues), 1);
SDError_ZeroOrderPS = zeros( length(Mvalues), 1);
SDError_SecondOrderPS = zeros( length(Mvalues), 1);
SDError_FourthOrderPS = zeros( length(Mvalues), 1);
SDError_ZeroOrderWSCOpt = zeros( length(Mvalues), 1);
SDError_SecondOrderWSCOpt = zeros( length(Mvalues), 1);
SDError_FourthOrderWSCOpt = zeros( length(Mvalues), 1);
SDLogError_ZeroOrderPS = zeros( length(Mvalues), 1);
SDLogError_SecondOrderPS = zeros( length(Mvalues), 1);
SDLogError_FourthOrderPS = zeros( length(Mvalues), 1);
SDLogError_ZeroOrderWSCOpt = zeros( length(Mvalues), 1);
SDLogError_SecondOrderWSCOpt = zeros( length(Mvalues), 1);
SDLogError_FourthOrderWSCOpt = zeros( length(Mvalues), 1);
MeanMoments = zeros( length(Mvalues), 2);

for s=1:length(Mvalues)
    M = Mvalues(s);
    temp_PS_0 = zeros( NumberSimulationsPerValue, 1);
    temp_PS_2 = zeros( NumberSimulationsPerValue, 1);
    temp_PS_4 = zeros( NumberSimulationsPerValue, 1);
    temp_WSC_0 = zeros( NumberSimulationsPerValue, 1);
    temp_WSC_2 = zeros( NumberSimulationsPerValue, 1);
    temp_WSC_4 = zeros( NumberSimulationsPerValue, 1);
    temp_Moments = zeros( NumberSimulationsPerValue, 2);
    for q=1:NumberSimulationsPerValue
        RandomDilationsWithOptimization_LinearSpacing
        temp_PS_0(q) = ErrorZeroOrderUnbiasingPS;
        temp_PS_2(q) = ErrorSecOrderUnbiasingPS;
        temp_PS_4(q) = ErrorFourthOrderUnbiasingPS;
        temp_WSC_0(q) = PSerror_ZeroOrderWSCOpt;
        temp_WSC_2(q) = PSerror_SecondOrderWSCOpt;
        temp_WSC_4(q) = PSerror_FourthOrderWSCOpt;
        temp_Moments(q,:) = Moments;
    end
    Error_ZeroOrderPS(s) = mean(temp_PS_0);
    Error_SecondOrderPS(s) = mean(temp_PS_2);
    Error_FourthOrderPS(s) = mean(temp_PS_4);
    Error_ZeroOrderWSCOpt(s) = mean(temp_WSC_0);
    Error_SecondOrderWSCOpt(s) = mean(temp_WSC_2);
    Error_FourthOrderWSCOpt(s) = mean(temp_WSC_4);
    SDError_ZeroOrderPS(s) = std(temp_PS_0);
    SDError_SecondOrderPS(s) = std(temp_PS_2);
    SDError_FourthOrderPS(s) = std(temp_PS_4);
    SDError_ZeroOrderWSCOpt(s) = std(temp_WSC_0);
    SDError_SecondOrderWSCOpt(s) = std(temp_WSC_2);
    SDError_FourthOrderWSCOpt(s) = std(temp_WSC_4);
    SDLogError_ZeroOrderPS(s) = std(log2(temp_PS_0));
    SDLogError_SecondOrderPS(s) = std(log2(temp_PS_2));
    SDLogError_FourthOrderPS(s) = std(log2(temp_PS_4));
    SDLogError_ZeroOrderWSCOpt(s) = std(log2(temp_WSC_0));
    SDLogError_SecondOrderWSCOpt(s) = std(log2(temp_WSC_2));
    SDLogError_FourthOrderWSCOpt(s) = std(log2(temp_WSC_4));
    MeanMoments(s,:) = mean(temp_Moments);
end



    
