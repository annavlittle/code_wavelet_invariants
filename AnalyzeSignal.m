function [FirstOrderWSC, w, PowerSpectrum, f_hat] = AnalyzeSignal(inputform,f,N,l,FilterBank)
% Possible syntaxes:
%[w, PowerSpectrum, FirstOrderWSC, f_hat] = AnalyzeSignal('Signal',f,N,l,FilterBank,OptimizationOpts)
%[w, PowerSpectrum, FirstOrderWSC] = AnalyzeSignal('PowerSpectrum',f,N,l,FilterBank,OptimizationOpts)
%Input:
% f: signal defined on [-N, N) with spacing 1/2^l (f has length 2^l*2N; f=f1+noise where f1 lives on [-N/2,N/2))
% AnalyzeSignalOpts.ScaleIncrements: structure which determines if the
% scales j for Wavelet scattering will have integer spacing or finer

% Define frequencies:

w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);

% Compute the power spectrum:

if strcmp(inputform,'Signal')
    % Compute fft of signal (note must shift [-N,N) to [0,2N) first):
    % Note: Use Delta t = 1/2^l instead of Delta t = 1 in computation of FT
    f_hat = ifftshift(fft(fftshift(f)))*(1/2^l);
    PowerSpectrum = abs(f_hat).^2;
elseif strcmp(inputform,'PowerSpectrum')
    PowerSpectrum = f;
end

% Compute First order wavelet scattering coefficients

PS_SqWavFT_Prod = abs(FilterBank.^2).*(ones(size(FilterBank,1),1)*PowerSpectrum);
FirstOrderWSC = ((1/(2*pi))*sum(PS_SqWavFT_Prod,2)*(pi/N))';


% % former options for all vs pos freq's:
% if strcmp(OptimizationOpts.Method,'Constrained')
%     
%     PS_SqWavFT_Prod = abs(FilterBank.^2).*(ones(size(FilterBank,1),1)*PowerSpectrum);
%     FirstOrderWSC = ((1/(2*pi))*sum(PS_SqWavFT_Prod,2)*(pi/N))';
%     
% elseif strcmp(OptimizationOpts.Method,'Unconstrained')
%     
%     % Compute the WSC using positive frequencies only:
%     nonneg_w_idx = (N*2^l+1):(2*N*2^l);
%     PS_SqWavFT_Prod = abs(FilterBank(:,nonneg_w_idx).^2).*(ones(size(FilterBank(:,nonneg_w_idx),1),1)*PowerSpectrum(nonneg_w_idx));
%     FirstOrderWSC = ((1/(2*pi))*sum(PS_SqWavFT_Prod,2)*(pi/N))';
% end

end

