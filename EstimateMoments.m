function [EstimatedMoments] = EstimateMoments(w,FourierTransform,PowerSpectrum,K,MomentEstimationOpts,N,l,noise_sigma)

% Input: frequencies w, Fourier Transform, highest even moment K to compute
% (K can equal 2 or 4)

M = size(FourierTransform,1);
alpha_0 = zeros(M,1);
alpha_1 = zeros(M,1);
for i=1:M
    if strcmp(MomentEstimationOpts.Method, 'FT')
        alpha_0(i) = sum(FourierTransform(i,end/2+1:end))*(pi/N);
        alpha_1(i) = sum(w(end/2+1:end).*FourierTransform(i,end/2+1:end))*(pi/N);
    elseif strcmp(MomentEstimationOpts.Method, 'PS')
        alpha_0(i) = sum(PowerSpectrum(i,end/2+1:end))*(pi/N);
        alpha_1(i) = sum(w(end/2+1:end).*PowerSpectrum(i,end/2+1:end))*(pi/N);
    end
end

g0 = noise_sigma^2*(2^(l+2))*pi*sinint((2^l)*N*pi);
g1 = noise_sigma^2*(2^(2*l+1))*(pi^2)*((2^(l+1))*N*pi*sinint((2^l)*N*pi)-1)/(3*N);

% first order moment approx
CV0 = (var(alpha_0) - g0)/abs(mean(alpha_0))^2;
CV1 = (var(alpha_1) - g1)/abs(mean(alpha_1))^2;

EstimatedMoments = zeros(1,K/2);

if MomentEstimationOpts.Order == 2
    EstimatedMoments(1) = CV0;
elseif MomentEstimationOpts.Order == 4
    EstimatedMoments(1) = (1/48)*(-13 + sqrt(169 + 2400*CV0 - 288*CV1));
end
if K==4
    B4 = (1/(2*(25*CV0 - 3*CV1)^2))*(1650*CV0^2 + CV1*(13 + sqrt(169 + 2400*CV0 - 288*CV1) + 18*CV1) - 4*CV0*(13 + sqrt(169 + 2400*CV0 - 288*CV1) + 87*CV1));
    EstimatedMoments(2) = B4*EstimatedMoments(1)^2;
end

% %% High additive noise method (requires estimating central frequency):
% 
% % Compute the central frequency of each signal:
% 
% Delta_w = w(2)-w(1);
% PosCentralFrequencies = zeros(1, size(PowerSpectrum,1));
% NegCentralFrequencies = zeros(1, size(PowerSpectrum,1));
% for i=1:size(PowerSpectrum,1)
%     PosCentralFrequencies(i) = sum(PowerSpectrum(i,end/2+1:end).*w(end/2+1:end))*Delta_w;
%     NegCentralFrequencies(i) = sum(PowerSpectrum(i,1:end/2).*w(1:end/2))*Delta_w;
% end
% 
% if abs(mean(PosCentralFrequencies)) >= abs(mean(NegCentralFrequencies))
%     ExpectedAddNoisePCF = (noise_sigma^2)*N*(2^(2*l))*pi^2;
%     EstimatedPCF = median(PosCentralFrequencies - ExpectedAddNoisePCF);
%     ExpectationRatio = (mean(PosCentralFrequencies) - ExpectedAddNoisePCF)/EstimatedPCF;
% else
%     ExpectedAddNoiseNCF = (noise_sigma^2)*N*(2^(2*l))*pi^2;
%     EstimatedNCF = median(-NegCentralFrequencies - ExpectedAddNoiseNCF); % This will be pos value
%     ExpectationRatio = (-mean(NegCentralFrequencies) - ExpectedAddNoiseNCF)/EstimatedNCF;
% end
% EstimatedMoments = zeros(1,K/2);
% EstimatedMoments(1) = (ExpectationRatio-1)/3;
% for i=4:2:K
%     EstimatedMoments(i/2) = 0
% end


end