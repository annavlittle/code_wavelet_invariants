% Randomly sample dilation factors:

Tau = zeros(M,1);
if strcmp(RandomDilationOpts.Distribution,'NoDilation')
    Tau = zeros(M,1);
elseif strcmp(RandomDilationOpts.Distribution,'Uniform')==1
    Tau=2*RandomDilationOpts.MagnitudeMaxTau*rand(M,1)-RandomDilationOpts.MagnitudeMaxTau;
elseif strcmp(RandomDilationOpts.Distribution,'TruncatedGaussian')==1
    pd = makedist('Normal');
    pd.mu = 0;
    pd.sigma = RandomDilationOpts.SDTruncatedGaussian;
    truncGaus = truncate(pd,-RandomDilationOpts.MagnitudeMaxTau,RandomDilationOpts.MagnitudeMaxTau);
    Tau = random(truncGaus,M,1);
end

t1=-(N/2):(1/2^l):(N/2)-1/2^l;
t = -(N):(1/2^l):(N)-1/2^l;

% Dilate Signals, either in space or frequency:

if strcmp(RandomDilationOpts.SynthesisDomain, 'Space')

    % Dilate Signals
    DilatedSignals = zeros(M,length(t1));
    if strcmp(RandomDilationOpts.Distribution,'NoDilation')==1 
        DilatedSignals = ones(M,1)*f1(t1);
    else
        DilatedSignals = (1./(1-Tau)).*DilateFunction(f1,t1,Tau);
    end

    % Pad with zeros:
    f = [zeros(1,(2^l)*N/2) f1(t1) zeros(1,(2^l)*N/2)];
    PaddedDilatedSignals = [zeros(M,(2^l)*N/2) DilatedSignals zeros(M,(2^l)*N/2)];
    if strcmp(RandomDilationOpts.Translate, 'True')
        for i=1:M
            rand_trans = randsample(length(t),1);
            PaddedDilatedSignals(i,:) = circshift( PaddedDilatedSignals(i,:), rand_trans );
        end
    end

    % Add Additive Noise
    NoisyPaddedDilatedSignals = PaddedDilatedSignals + true_noise_sigma*sqrt(2^l)*randn( size(PaddedDilatedSignals) );

    % Compute Power Spectrum and First Order Wavelet Scattering Coefficients
    % for Pure Signal
    [UndilatedFirstOrderWSC, w, UndilatedPowerSpectrum, Undilatedf_hat] = AnalyzeSignal('Signal',f,N,l,FilterBank);

    % Compute Power Spectrum and First Order Wavelet Scattering Coefficients
    % for Dilated Signals
    PowerSpectrum = zeros(M, (2^l)*2*N );
    FourierTransform = zeros(M, (2^l)*2*N );
    FirstOrderWSC = zeros(M, length(j) );
    for i=1:M
        [FirstOrderWSC(i,:), w, PowerSpectrum(i,:), FourierTransform(i,:)] = AnalyzeSignal('Signal', NoisyPaddedDilatedSignals(i,:),N,l,FilterBank);
    end
    
elseif strcmp(RandomDilationOpts.SynthesisDomain, 'Frequency')  
    
    w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);
    Undilatedf_hat = f1(w);
    UndilatedPowerSpectrum = abs(Undilatedf_hat).^2;
    f = ifftshift(ifft(ifftshift(Undilatedf_hat)))*2^l;
    
    FourierTransform = zeros(M,length(w));
    if strcmp(RandomDilationOpts.Distribution,'NoDilation')==1 
        FourierTransform = ones(M,1)*Undilatedf_hat + true_noise_sigma*randn( size(FourierTransform) );
    else
        FourierTransform = DilateFunction(f1,w,Tau./(Tau-1)) + true_noise_sigma*sqrt(2*N)*randn( size(FourierTransform) );
    end
    PowerSpectrum = abs(FourierTransform).^2;

    % Compute First Order Wavelet Scattering Coefficients for Pure Signal
    [UndilatedFirstOrderWSC, w] = AnalyzeSignal('PowerSpectrum',UndilatedPowerSpectrum,N,l,FilterBank);

    % First Order Wavelet Scattering Coefficients for Noisy Dilated Signals

    FirstOrderWSC = zeros(M, length(j) );
    for i=1:M
        [FirstOrderWSC(i,:)] = AnalyzeSignal('PowerSpectrum', PowerSpectrum(i,:),N,l,FilterBank);
    end
    
end

% Make the wavelet filter bank in space
WaveletsSpace = zeros(size(FilterBank));
for i=1:length(j)
    psihat = FilterBank(i,:);
    WaveletsSpace(i,:) = ifftshift(ifft(ifftshift(psihat)))*2^l;
end

% Scale by j=0
if strcmp(FilterBankOpts.FilterBank,'Gabor')==1
    MotherWaveletFT = LPConstant*GaborWaveletFreq(N, l, 4/pi, 3*pi/4, 0)/sqrt(sum(abs(GaborWaveletFreq(N, l, 4/pi, 3*pi/4, 0).^2)));
elseif strcmp(FilterBankOpts.FilterBank,'Morlet')==1
    MotherWaveletFT = LPConstant*MorletWaveletFreq(N, l, 4/pi, 3*pi/4, 0)/sqrt(sum(abs(MorletWaveletFreq(N, l, 4/pi, 3*pi/4, 0).^2)));
elseif strcmp(FilterBankOpts.FilterBank,'CubicSpline')==1
    MotherWaveletFT = LPConstant*CubicSplineWaveletFreq(N, l, 0)/sqrt(sum(abs(CubicSplineWaveletFreq(N, l, 0).^2)));
end
MotherWaveletSpace = ifftshift(ifft(ifftshift(MotherWaveletFT)))*2^l;
psi_L2norm_sq = sum(abs(MotherWaveletSpace).^2)*(1/2^l);


%% For each freq w, scale j, compute the mean of the samples

MeanPowerSpectrum=mean(PowerSpectrum);
MeanFirstOrderWSC=mean(FirstOrderWSC);

% Empirically estimate the additive noise variance:
estimated_noise_sigma = sqrt(mean([MeanPowerSpectrum(1:N) MeanPowerSpectrum(end-N+1:end)])/(2*N));
if true_noise_sigma == 0
    noise_sigma = 0; %Do not do additive noise unbiasing if there is no additive noise
elseif strcmp(RandomDilationOpts.MomentCalc,'Empirical')==1
    noise_sigma = estimated_noise_sigma;
else
    noise_sigma = true_noise_sigma;
end

%% Empirically estimate the first k even moments of the dilation distribution of tau:

if strcmp(RandomDilationOpts.Distribution, 'NoDilation')
    Moments = [0 0];
elseif strcmp(RandomDilationOpts.MomentCalc,'Empirical')==1
    Moments = EstimateMoments(w,FourierTransform,PowerSpectrum,4,MomentEstimationOpts,N,l,noise_sigma)
    TrueMoments = [moment(Tau,2) moment(Tau,4)]
elseif strcmp(RandomDilationOpts.MomentCalc,'Oracle')==1
    Moments = [moment(Tau,2) moment(Tau,4)];
end
    
% Estimate the coefficients needed in unbiasing from the moments;
if strcmp(RandomDilationOpts.Distribution,'NoDilation')
    B2 = 0;
    B4 = 0;
else
    B2 = 1/2;
    B4 = Moments(2)/(24*(Moments(1))^2) - 1/4;
end

%% Unbias the Power Spectrum Using the Derivatives

Deltaw = abs(w(2)-w(1));
SecondDerivPowerSpectrum = diff2_6O_FCD(MeanPowerSpectrum,Deltaw);
FourthDerivPowerSpectrum = diff4_4O_FCD(MeanPowerSpectrum,Deltaw);

SecondOrderUnbiasTerm_PS = B2*Moments(1)*(w(4:end-3).^2).*SecondDerivPowerSpectrum;
FourthOrderUnbiasTerm_PS = (B4*(Moments(1))^2).*(w(4:end-3).^4).*FourthDerivPowerSpectrum;

ZeroOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - 2*N*noise_sigma^2;
SecondOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - SecondOrderUnbiasTerm_PS - 2*N*noise_sigma^2;
FourthOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - SecondOrderUnbiasTerm_PS - FourthOrderUnbiasTerm_PS - 2*N*noise_sigma^2;

ErrorZeroOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - ZeroOrderUnbiasedPowerSpectrum)/sqrt(2^l)
ErrorSecOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - SecondOrderUnbiasedPowerSpectrum)/sqrt(2^l)
ErrorFourthOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - FourthOrderUnbiasedPowerSpectrum)/sqrt(2^l)


%% Unbias the Wavelet Coefficients Using the Derivatives

lam=2.^j;
Delta_lam = abs(lam(2)-lam(1));

% % Take Derivatives wrt j (use 2nd and 4th order methods):
% FirstDerivFirstOrderWSC = diff1_4O_FCD(MeanFirstOrderWSC,Delta_lam);
% SecondDerivFirstOrderWSC = diff2_4O_FCD(MeanFirstOrderWSC,Delta_lam);
% ThirdDerivFirstOrderWSC = diff3_2O_FCD(MeanFirstOrderWSC,Delta_lam);
% FourthDerivFirstOrderWSC = diff4_2O_FCD(MeanFirstOrderWSC,Delta_lam);

% Take Derivatives wrt j (use 4th and 6th order methods):
SecondDerivFirstOrderWSC = diff2_6O_FCD(MeanFirstOrderWSC,Delta_lam);
FourthDerivFirstOrderWSC = diff4_4O_FCD(MeanFirstOrderWSC,Delta_lam);

% Compute \lambda^k Sf^{(k)}(\lambda), i.e. convert to match our theoretical form
SecondOrderUnbiasTerm = B2*Moments(1)*(lam(4:end-3).^2).*SecondDerivFirstOrderWSC;
FourthOrderUnbiasTerm = B4*(Moments(1))^2*(lam(4:end-3).^4).*FourthDerivFirstOrderWSC;

ZeroOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(4:end-3) - (psi_L2norm_sq)*2*N*noise_sigma^2;
SecondOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(4:end-3) - SecondOrderUnbiasTerm - (psi_L2norm_sq)*2*N*noise_sigma^2;
FourthOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(4:end-3) - SecondOrderUnbiasTerm - FourthOrderUnbiasTerm - (psi_L2norm_sq)*2*N*noise_sigma^2;

ErrorZeroOrderUnbiasing = norm(UndilatedFirstOrderWSC(4:end-3) - ZeroOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam)
ErrorSecOrderUnbiasing = norm(UndilatedFirstOrderWSC(4:end-3) - SecondOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam)
ErrorFourthOrderUnbiasing = norm(UndilatedFirstOrderWSC(4:end-3) - FourthOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam)


%% Now Run Optimization to see if can recover true power spectrum from these features

opt_lam = lam(4:end-3);    

% Initialize optimization w/ mean PS:
if strcmp(OptimizationOpts.Initialization,'MeanPS_NoDilUnbias')
    TargetPowerSpectrum = MeanPowerSpectrum - 2*N*noise_sigma^2;
elseif strcmp(OptimizationOpts.Initialization,'MeanPS_Order2Unbias')
    TargetPowerSpectrum = SecondOrderUnbiasedPowerSpectrum; %seems like this will cause error in current impl. b/c freq dim's won't match Filter Bank
elseif strcmp(OptimizationOpts.Initialization,'MeanPS_Order4Unbias')
    TargetPowerSpectrum = FourthOrderUnbiasedPowerSpectrum; %seems like this will cause error in current impl. b/c freq dim's won't match Filter Bank
end

if strcmp(OptimizationOpts.Method,'Constrained')

    % Positivity constraint (inequality):
    A = -eye(length(t));
    b=zeros(length(t),1);

    % Symmetry constraint (equality):
    % Constraint 2: function must be even/symmetric, so that FT is real
    Aeq = zeros(length(t),length(t));
    Aeq(2:end,2:end) = eye(length(t)-1) - fliplr(eye(length(t)-1));
    beq = zeros(length(t),1);

    %options = optimoptions('fmincon','Algorithm','sqp','SpecifyObjectiveGradient',true,'HessianFcn','objective','MaxFunctionEvaluations', 100000,'MaxIterations',100000,'StepTolerance', 1e-6,'FunctionTolerance', 1e-6, 'PlotFcn','optimplotx','OptimalityTolerance',1e-6,'ConstraintTolerance',1e-6,'Display','iter');
    tol = OptimizationOpts.tol;
    f0 = TargetPowerSpectrum;
    % Initialize optimization randomly initialize:
    %f0 = abs(fft(.05*randn(size(f)))*(1/2^l)).^2;
    options = optimoptions('fmincon','Algorithm','sqp','SpecifyObjectiveGradient',true,'HessianFcn','objective','MaxFunctionEvaluations', 100000,'MaxIterations',100000,'StepTolerance', tol,'FunctionTolerance', tol,'OptimalityTolerance',tol,'ConstraintTolerance',tol,'Display','iter');
    
    % Optimization with zero order WSC:
    fun = @(fPS)compute_loss_PS_with_gradhess(fPS,N,l,ZeroOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:end-3,:));
    [ZeroOrder_recoveredfPS,lossval] = fmincon(fun,f0,A,b,Aeq,beq,[],[],[],options); %Initialize w/ Mean PS
    
    % Optimization with second order WSC:
    fun = @(fPS)compute_loss_PS_with_gradhess(fPS,N,l,SecondOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:end-3,:));
    [SecondOrder_recoveredfPS,lossval] = fmincon(fun,f0,A,b,Aeq,beq,[],[],[],options); %Initialize w/ Mean PS
    
    % Optimization with fourth order WSC:
    fun = @(fPS)compute_loss_PS_with_gradhess(fPS,N,l,FourthOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:end-3,:));
    [FourthOrder_recoveredfPS,lossval] = fmincon(fun,f0,A,b,Aeq,beq,[],[],[],options); %Initialize w/ Mean PS
    
elseif strcmp(OptimizationOpts.Method,'Unconstrained')  

    % Recompute the target WSC on using just the nonnegative freq's:
    nonneg_w_idx = (N*2^l+1):(2*N*2^l);
    
    % Filter Bank Flip Implementation:
    
    % Specify gradient:
    tol = OptimizationOpts.tol;
    f0 = sqrt(abs(TargetPowerSpectrum(nonneg_w_idx)));
    options = optimoptions('fminunc','Algorithm','quasi-newton','SpecifyObjectiveGradient',true,'MaxFunctionEvaluations', 100000,'MaxIterations',100000,'StepTolerance', tol,'FunctionTolerance', tol,'OptimalityTolerance',tol,'Display','iter');
    
    % Optimization with zero order WSC:
    fun = @(fFT)compute_loss_FT_with_grad_flipFilterBank(fFT,N,l,ZeroOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:size(FilterBank,2)-3,:));
    [recoveredfFT,lossval,exitflag,output,grad,hessian]=fminunc(fun,f0,options);
    fullrecoveredfFT = [fliplr(recoveredfFT) recoveredfFT]; % Extend optimization solution to all freq's:
    ZeroOrder_recoveredfPS = fullrecoveredfFT.^2;  % Recovered power spectrum:  
    PSerror_ZeroOrderWSCOpt = norm(UndilatedPowerSpectrum(4:end-3) - ZeroOrder_recoveredfPS(4:end-3))/sqrt(2^l) %restricting to 4:end-3 for fair comparison w/ PS method
    
    % Optimization with second order WSC:
    fun = @(fFT)compute_loss_FT_with_grad_flipFilterBank(fFT,N,l,SecondOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:size(FilterBank,2)-3,:));
    [recoveredfFT,lossval,exitflag,output,grad,hessian]=fminunc(fun,f0,options);
    fullrecoveredfFT = [fliplr(recoveredfFT) recoveredfFT]; % Extend optimization solution to all freq's:
    SecondOrder_recoveredfPS = fullrecoveredfFT.^2;  % Recovered power spectrum
    PSerror_SecondOrderWSCOpt = norm(UndilatedPowerSpectrum(4:end-3) - SecondOrder_recoveredfPS(4:end-3))/sqrt(2^l) %restricting to 4:end-3 for fair comparison w/ PS method
    
    % Optimization with fourth order WSC:
    fun = @(fFT)compute_loss_FT_with_grad_flipFilterBank(fFT,N,l,FourthOrderUnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(4:size(FilterBank,2)-3,:));
    [recoveredfFT,lossval,exitflag,output,grad,hessian]=fminunc(fun,f0,options);
    fullrecoveredfFT = [fliplr(recoveredfFT) recoveredfFT]; % Extend optimization solution to all freq's:
    FourthOrder_recoveredfPS = fullrecoveredfFT.^2;  % Recovered power spectrum: 
    PSerror_FourthOrderWSCOpt = norm(UndilatedPowerSpectrum(4:end-3) - FourthOrder_recoveredfPS(4:end-3))/sqrt(2^l) %restricting to 4:end-3 for fair comparison w/ PS method

end

if RandomDilationOpts.WSCUnbiasingOrder == 4
    % Try to recover signal from WSC recovered power spectrum (only works for real,even signal with positive FT)
    recoveredf = ifftshift(ifft(fftshift(sqrt(FourthOrder_recoveredfPS))))*2^l; 
    recoveredfPS = FourthOrder_recoveredfPS;
    TargetFirstOrderWSC = FourthOrderUnbiasedFirstOrderWSC;
elseif RandomDilationOpts.WSCUnbiasingOrder == 2
    % Try to recover signal from WSC recovered power spectrum (only works for real,even signal with positive FT)
    recoveredf = ifftshift(ifft(fftshift(sqrt(SecondOrder_recoveredfPS))))*2^l;
    recoveredfPS = SecondOrder_recoveredfPS;
    TargetFirstOrderWSC = SecondOrderUnbiasedFirstOrderWSC;
elseif RandomDilationOpts.WSCUnbiasingOrder == 0
    % Try to recover signal from WSC recovered power spectrum (only works for real,even signal with positive FT)
    recoveredf = ifftshift(ifft(fftshift(sqrt(ZeroOrder_recoveredfPS))))*2^l;
    recoveredfPS = ZeroOrder_recoveredfPS;
    TargetFirstOrderWSC = ZeroOrderUnbiasedFirstOrderWSC;
end

% Try to recover signal from Avg PS (only works for real,even signal with positive FT)
recoveredf_AvgPS = ifftshift(ifft(fftshift(sqrt(TargetPowerSpectrum))))*2^l;

% Scattering Coefficents of Recovered Signal:
[RecoveredFirstOrderWSC] = AnalyzeSignal('PowerSpectrum',recoveredfPS,N,l,FilterBank(4:end-3,:));
    
    