% WARNING: do not run MakeFigures all at once, as generating all figures takes a large amount of time and computing power. 

addpath(genpath('Figure_scripts'));

%% Scripts for Figure 2

fig2_a
fig2_b
fig2_b_demo
fig2_c
fig2_c_demo

%% Scripts for Figure 3

fig3_a_c
fig3_b_d

%% Scripts for Figure 4

fig4_a
fig4_a_demo
fig4_b
fig4_b_demo
fig4_c
fig4_c_demo
fig4_d
fig4_d_demo
fig4_e
fig4_e_demo
fig4_f
fig4_f_demo

%% Scripts for Figure 5

fig5_a
fig5_a_demo
fig5_b
fig5_b_demo
fig5_c
fig5_c_demo
fig5_d
fig5_d_demo
fig5_e
fig5_e_demo
fig5_f
fig5_f_demo
fig5_g
fig5_g_demo
fig5_h
fig5_h_demo
fig5_i
fig5_i_demo
fig5_j
fig5_j_demo
fig5_k
fig5_k_demo
fig5_l
fig5_l_demo

%% Scripts for Figure 6

fig6

%% Scripts for Figure G7

figG7_a
figG7_a_demo
figG7_b
figG7_b_demo
figG7_c
figG7_c_demo
figG7_d
figG7_d_demo
figG7_e
figG7_e_demo
figG7_f
figG7_f_demo
figG7_g
figG7_g_demo
figG7_h
figG7_h_demo
figG7_i
figG7_i_demo
figG7_j
figG7_j_demo
figG7_k
figG7_k_demo
figG7_l
figG7_l_demo

%% Scripts for Figure H8

figH8_a
figH8_a_demo
figH8_b
figH8_b_demo
figH8_c
figH8_c_demo
figH8_d
figH8_d_demo
figH8_e
figH8_e_demo
figH8_f
figH8_f_demo
figH8_g
figH8_g_demo
figH8_h
figH8_h_demo
figH8_i
figH8_i_demo
figH8_j
figH8_j_demo
figH8_k
figH8_k_demo
figH8_l
figH8_l_demo